package com.mycompany.dvdstore.controller;

import com.mycompany.dvdstore.entity.Movie;
import com.mycompany.dvdstore.service.MovieServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.Scanner;

@Controller
public class MovieController {
  @Autowired
  private MovieServiceInterface movieService;

  public void addUsingConsole() {
    Scanner sc;
    Movie movie;
    String movieGenre;
    String movieTitle;

    sc = new Scanner(System.in);
    System.out.println("Quel est le titre du film que vous souhaiter ajouter ?");
    movieTitle = sc.nextLine();
    System.out.println("Quel est le genre de ce film ?");
    movieGenre = sc.nextLine();

    movie = new Movie(movieTitle, movieGenre);
    this.movieService.registerMovie(movie);
  }
}
