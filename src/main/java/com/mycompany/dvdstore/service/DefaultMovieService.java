package com.mycompany.dvdstore.service;

import com.mycompany.dvdstore.entity.Movie;
import com.mycompany.dvdstore.repository.MovieRepositoryInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DefaultMovieService implements MovieServiceInterface {
  @Autowired
  private MovieRepositoryInterface movieRepo;

  public void registerMovie(Movie movie) {
    this.movieRepo.add(movie);
  }
}
